#!/usr/bin/env python

#
# Run with python
#

import csv
import json
import pytz
from datetime import datetime
#import markdown
from jinja2 import Environment, FileSystemLoader, Markup, select_autoescape

env = Environment(
        loader = FileSystemLoader('templates'),
        autoescape=select_autoescape(['html', 'xml']),
        extensions=['jinja2_highlight.HighlightExtension']
        )

env.extend(jinja2_highlight_cssclass = 'highlight')

common_pages = [
        'accepted',
        'ama',
        'awards',
        'cfp',
        'cfw',
        'cfs',
        'cfss',
        'cfsrc',
        'cft',
        'code',
        'cc',
        'dei',
        'diversity',
        'final',
        'index',
        'mentoring',
        'program',
        'reg',
        'sched',
        'speakers',
        'sponsors',
        'sponsor_sessions',
        'tutorials',
        'venue',
        'virtual',
        'visa',
        'workshops',
]

for p in common_pages:
    print("Processing " + p)
    template = env.get_template(p + ".html")
    file = open(p + ".html", "w")
    file.write(template.render(page = p, title = ""))
    file.close()

pchtml = ""
with open('pc.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    col_name = 0
    col_affiliation = 1
    r = []
    for row in csv_reader:
        if len(row) > 0:
            r.append(row)
    def getK(r):
        return r[col_name].split(" ")[1]
    rows = sorted(r, key=getK)
    row = rows[0]
    pchtml +=  '      <tr>\n'
    pchtml += f'        <td class=organizers-office> Program Committee</td>\n'
    pchtml += f'        <td class=organizers-bearer> {row[col_name]}, {row[col_affiliation]}</td>\n' % row
    pchtml +=  '      </tr>\n'
    for row in sorted(r, key=getK)[1:]:
        pchtml +=  '      <tr>\n'
        pchtml += f'        <td class=organizers-bearer> &nbsp;</td>\n'
        pchtml += f'        <td class=organizers-bearer> {row[col_name]}, {row[col_affiliation]}</td>\n' % row
        pchtml +=  '      </tr>\n'

volunteershtml = ""
with open('volunteers.csv') as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=',')
    col_name = 0
    col_affiliation = 1
    r = []
    for row in csv_reader:
        if len(row) > 0:
            r.append(row)
    def getK(r):
        return r[col_name].split(" ")[1]
    rows = sorted(r, key=getK)
    row = rows[0]
    volunteershtml +=  '      <tr>\n'
    volunteershtml += f'        <td class=organizers-office> Student Volunteers</td>\n'
    volunteershtml += f'        <td class=organizers-bearer> {row[col_name]}, {row[col_affiliation]}</td>\n' % row
    volunteershtml +=  '      </tr>\n'
    for row in sorted(r, key=getK)[1:]:
        volunteershtml +=  '      <tr>\n'
        volunteershtml += f'        <td class=organizers-bearer> &nbsp;</td>\n'
        volunteershtml += f'        <td class=organizers-bearer> {row[col_name]}, {row[col_affiliation]}</td>\n' % row
        volunteershtml +=  '      </tr>\n'

# Organizers
template = env.get_template('organizers.html')
file = open("organizers.html", "w")
file.write(template.render(page = "organizers",
                           title = "",
                           pcorganizers = Markup(pchtml),
                           volunteers = Markup(volunteershtml)))
file.close()


program = json.load(open("templates/program.json", "r"))
times = json.load(open("templates/session_times.json", "r"))


def get_program_html(program):
    session_html = {}
    for session in program:
        session_html["session_%d" % session["id"]] = Markup(get_session_html(session) + "\n")
    t = env.get_template("program.html")
    bbreak = Markup('<tr class="success"><td><strong>Break</strong></td></tr><tr><td></td></tr>')
    return t.render(
        page = "program", 
        bbreak = bbreak,
        **session_html)

def format_author(a):
    return "%s %s (%s)" % (a["first"], a["last"], a["affiliation"])

def get_session_html(session):
    mirror_1 = times["mirror_1"]["session_%d" % session["id"]]
    mirror_2 = times["mirror_2"]["session_%d" % session["id"]]

    phtml = ""
    for i, paper in enumerate(session["papers"]):
        t1 = i * 1200000 + int(datetime.fromisoformat(mirror_1["begin"]).timestamp() * 1000)
        t2 = i * 1200000 + int(datetime.fromisoformat(mirror_2["begin"]).timestamp() * 1000)
        phtml += get_paper_html(paper, t1, t2) + "\n"

    def session_times(d):
        begin = datetime.fromisoformat(d["begin"]).strftime("%H:%M")
        end = datetime.fromisoformat(d["end"]).strftime("%H:%M (%Z)")
        return "%s - %s" % (begin, end)

    t = env.get_template('program_session.html')
    return t.render(
        id=session["id"], 
        name=session["name"], 
        time = session_times(mirror_1),
        time2 = session_times(mirror_2),
        begin1 = int(datetime.fromisoformat(mirror_1["begin"]).timestamp() * 1000),
        end1 = int(datetime.fromisoformat(mirror_1["end"]).timestamp() * 1000),
        begin2 = int(datetime.fromisoformat(mirror_2["begin"]).timestamp() * 1000),
        end2 = int(datetime.fromisoformat(mirror_2["end"]).timestamp() * 1000),
        chair1 = format_author(session["chairs"][0]),
        chair2 = format_author(session["chairs"][1]),
        papers=Markup(phtml))


def get_paper_html(paper, t1, t2):
    authors = ", ".join([format_author(a) for a in paper["authors"]])
    t = env.get_template('program_paper.html')

    hasvideo = ""
    if paper["video"] == "":
        hasvideo=" style='display: none;' "

    haslongvideo = ""
    if paper["longvideo"] == "":
        haslongvideo=" style='display: none;' "

    award = ""
    if "award" in paper:
        award = "<br/><i><img class='icon-paper' src='images/trophy.png'/> <b>" + paper["award"] + "</b></i>"

    return t.render(
        title=paper["title"], 
        authors=authors,
        t1=t1,
        t2=t2,
        url=paper["url"],
        video=paper["video"],
        longvideo=paper["longvideo"],
        hasvideo=Markup(hasvideo),
        award=Markup(award),
        haslongvideo=Markup(haslongvideo)
    )


programtemplate = env.get_template("program.html")
file = open("program.html", "w")
file.write(get_program_html(program))
file.close()



# t2 = env.get_template('program_paper.html')
# t2r = t2.render(papertitle="blah", paperurl="http://me")
# print(t2r)

# t3 = env.get_template('program_session.html')
# print(t3.render(id=5, name="My session", papers=Markup(t2r)))




