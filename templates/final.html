{# vim: ts=2:sw=2:expandtab #}

{% extends "base.html" %}

{% block content %}
<div class="main-text">
  <div class="welcome">
    <h3>Instructions for Authors</h3>
    <div class="col-md-12 welcome-middle">
      <h5>Submission</h5>
      <p>
      The camera-ready version of your paper should be submitted via HotCRP.  This 
      will include all your sources and the generated PDF.  Both the final paper
      and the signed copyright form are due by the camera-ready deadline.
      </p>
    
      <h5>Formatting</h5>
      <p>
      Camera-ready papers can have 12 pages of technical content, including
      all text, figures, tables, etc.  Your shepherd can approve up to two
      additional pages of technical content.  Bibliographic references are not 
      included in the 12-page limit. You must use the US letter paper size, with
      all text and figures fitting inside a 178 x 229 mm (7 x 9 in) block centered
      on the page, using two columns separated by 8 mm (0.33) of whitespace. Use
      10-point font (typeface Times Roman, Linux Libertine, etc.) on 12-point 
      (single-spaced) leading. Graphs and figures should be readable when printed 
      in grayscale, without magnification. Do not include page numbers,
      footers, or headers.  References within the paper should be
      hyperlinked.
      </p>

      <p>
      Paper titles should be in Initial Caps Meaning First Letter of the
      Main Words Should be Made Capital Letters.  As an example, refer
      to the previous sentence: note the Capital Letter "M" in Must,
      Meaning, and Main, the Capital Letter "C" in Caps and Capital and
      the Capital Letter "L" in Letter and Letters.
      </p>

      <p>
      Most of these rules are automatically applied when using the official
      LaTeX or MS Word templates from the ACM, available
      <a href="https://www.acm.org/publications/proceedings-template">here</a>.
      </p>
    
      <p>
      Please follow the font requirements from part 8 of the
      <a href="https://www.acm.org/publications/gi-proceedings-current">ACM
      guidelines for proceedings</a>.  In particular, papers should have Type 1
      fonts (scalable), not Type 3 (bit-mapped). All fonts MUST be embedded
      within the PDF file.  This will usually happen by default when using
      <tt>pdflatex</tt>, but in rare cases, PDFs generated with <tt>pdflatex</tt>
      will not contain embedded fonts. This most often occurs when vector images
      included in paper do not themselves embed their fonts.
      </p>
    
      <p>
      Authors should make sure to use the ACM classification system. For details 
      see
      <a href="https://dl.acm.org/ccs/ccs.cfm">https://dl.acm.org/ccs/ccs.cfm</a>.
      </p>

      <p>
      Be sure that there are no bad page or column breaks, meaning no widows (last 
      line of a paragraph at the top of a column). Section and Sub-section heads 
      should remain with at least 2 lines of body text when near the end of a page 
      or column.
      </p>

      <p>
      When submitting the camera-ready paper, make sure the author full names and
      affiliations in HotCRP are up-to-date, and that the abstract in HotCRP is
      also updated.
      </p>
    
      <h5>Copyright Block</h5>
    
      <p>
      In its bottom-left corner, the first page of the final paper must include a 
      copyright notice.
      </p>
    
      <p>
      Author will receive the relevant copyright information after they have filled 
      out the e-Rights form via the ACM rights-management tool on the HotCRP page 
      of the paper.
      </p>
    
      <p>
      ACM will use its automated system to email this information to the authors.  
      Please set your email spam settings to allow messages from 
      "rightsreview@acm.org". The copyright block includes the DOI specific to the 
      paper, rights-management text that depends on the author's choice of a 
      license or copyright transfer, the official name, dates, and location of the 
      event.
      </p>

      <p>
      For papers typeset using MS Word, the copyright block statement must be in
      7-point Libertine font, with the first paragraph text justified, and with the
      venue information in italics. Refer to the <tt>interim-layout.docx</tt> sample
      file from the <a href="https://www.acm.org/publications/proceedings-template">ACM
      template page</a> to view how the ACM copyright statement should appear; you
      should preserve the original formatting.
      </p>

      <h5>Images and Figures</h5>

      <p>
      Below are some recommendations to ensure good print reproduction of the images, figures, and illustrations utilized in your submission.
      </p>

      <p>
      <b>Colors and Black &amp; White (Gray Scale) Print Testing.</b>
      If you have any images in color, please print your paper out in
      black and white to ensure that the tones and screens used in your
      images or figures reproduce well in black and white, too. However,
      your images will appear in full color in any distributed electronic
      proceedings and in the ACM digital library.
      </p>

      <p>
      <b>Resolution &amp; CMYK.</b> Figures, charts, and diagrams
      should use a vector image format (e.g. PDF, SVG). Raster images
      (e.g. photographs) should be at least 300 or 600 dpi for quality
      reproduction and saved as TIFF images (or other compatible
      formats that support print-quality resolution). When creating
      or revising your images for inclusion in the paper, we recommend
      choosing CMYK (and not RGB) as the color profile.
      </p>

      <p>
      <b>TIFF/PNG versus JPG (JPEG) image.</b> For raster images
      (e.g. photos), TIFFs are preferred for press applications where
      quality takes priority over file size. When TIFFs are compressed
      (using LZW compression option when saving from Adobe Photoshop, for
      example), no image data is lost, thus ensuring maximum quality. A
      JPEG is a compressed image format designed to keep the file size
      small, which makes it ideal for use in web graphics. However,
      to achieve this, the JPEG format actually removes precision from
      the image. This is referred to as a lossy compression system. On
      a printout, the removed data tends to show up as blocky areas of
      a solid color, or ghosting near high-contrast changes. At higher
      print resolutions (a minimum of 200 dpi), there’s usually enough
      data in the JPEG file for the compression artefacts to be very
      noticeable. PNG files are also a good alternative as the format
      is also lossless.
      </p>

      <p>
      <b>Rules/Lines.</b> Rules used in your graphs, tables or charts
      must be at least 0.5 point in stroke and black for quality
      reproduction. Finer lines and points than this will not reproduce
      well, even if you can see them on your laser printed hardcopy
      when checked -- your laser printers will usually have a far lower
      resolution than the imagesetters that will be used.
      </p>

      <p>
      <b>Fonts.</b> If your figure uses custom or any non-standard font,
      the characters may appear differently when printed in the
      proceedings. Remember to check your figure creation to ensure that
      all fonts are embedded or included in the figure correctly. Be
      sure that your images do not contain any Type 3 fonts.
      </p>

      <p>
      <b>Transparency.</b> If a figure or image is assembled from
      multiple images, the images must be embedded, and layers be
      flattened or grouped together properly in the file. Transparency
      must be flattened.
      </p>

      <h5>Third-Party Material and Acknowledgements</h5>

      <p>
      In the event any element used in your material contains the work
      of third-parties, it is the authors' responsibility to secure any
      necessary permissions and/or licenses, and the authors will provide
      the same permissions in writing to the ACM. If the copyright holder
      requires a citation to a copyrighted work, it is the authors'
      responsibility to include the correct wording and citations to
      the copyrighted material in their submissions.
      </p>

      <p>
      It is the authors' responsibility to be sure
      that any funding or special contribution acknowledgements are included
      in the final version submitted as required by any research, financial,
      or other grants received (typically by using an "Acknowledgements" section
      before the "References" section).
      </p>

      <h5>Appendix</h5>
      <p>
      This year we would like to experiment with allowing additional
      material in appendices of the camera-ready version of the
      paper.  If you are interested in including an appendix in your
      camera-ready version, you must send an email to your shepherd
      <emph>and</emph> the program chairs by August 27th, 2021, stating
      what you are looking to include, and why the appendix is the right
      way to include this material.  We will determine whether your
      proposed use of an appendix makes sense, and also use this to
      inform future policies on the use of appendices for SOSP.
      </p>

      <p>
      Appendices should be used for supplementary material that is not
      required for understanding your paper; much as we required in the
      call for papers, your camera-ready paper should stand on its own
      without the appendix.  An appendix may be a good place to publish
      formal proofs, detailed analysis, or full algorithms.  There are
      also other ways to distribute additional material for a paper,
      such as a technical report, an arXiv publication, posting code
      or data on Github, posting supplementary materials on the ACM
      digital library, etc.  One advantage of the appendix is that
      it is readily available to readers of your paper to refer to.
      </p>

      <p>
      Any approved use of appendices will be required to clearly
      indicate that the appendix has not been peer-reviewed.
      </p>

      <h5>Shepherding</h5>
      <p>
      Every accepted paper is subject to shepherding.  You must receive explicit
      approval from your shepherd by the camera-ready deadline.  Please plan
      accordingly, by providing a draft of your camera-ready paper to the shepherd
      for review and feedback well in advance of the final deadline.  Papers that
      do not receive shepherd approval will not appear in the conference.
      </p>

      <h5>Video Recording</h5>
      <p>
      In addition to the camera-ready paper, please record a video
      presenting your paper, which will be used in the virtual conference.
      We will post all videos online ahead of the conference, so that
      attendees can watch videos before attending.  Your video will
      also be broadcast to all attendees during the conference as part
      of the session that your paper is assigned to.
      </p>

      <p>
      Your video should be approximately 10 minutes long.  Since it
      can be difficult to make the video exactly 10 minutes, we will
      allow some slack, rejecting videos that are 11 minutes or longer.
      We also ask that authors participate in the online Q&amp;A following
      the presentation of their paper and video during the conference.
      </p>

      <p>
      Optionally, authors can also record a longer video.  The longer
      video will be posted online before the conference for attendees
      to watch, but will not be broadcast synchronously during the
      conference itself.  We suggest aiming for about 20 minutes, but
      this is more flexible.
      </p>

      <p>
      Your videos should be in an MP4 file format using the H.264 codec
      at 1920x1080 resolution (1080p, 16:9 aspect ratio).  Please use a
      headset if needed to achieve good quality audio.  The maximum file
      size for your videos to be uploaded is 600 MBytes.
      </p>

      <p>
      Please upload your video recording(s) and the slides used for your
      10-minute video in HotCRP.  The deadline for uploading your video
      is October 11, 2021.
      </p>

      <h5 class="left-h">Questions</h5>
      <p>
      Any questions about camera-ready papers can be emailed to the PC chairs at
      <a href="mailto:sosp21-pc-chairs@mpi-sws.org">sosp21-pc-chairs@mpi-sws.org</a>
      and/or to the proceedings chairs,
      <a href="mailto:rodrigo.miragaia.rodrigues@tecnico.ulisboa.pt,luis.veiga@inesc-id.pt?subject=[SOSP-21-Procs]">Rodrigo and Luis</a>.
      </p>
    </div>
    <div class="clearfix"> </div>
  </div>
</div>
{% endblock %}
