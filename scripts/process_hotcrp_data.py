import argparse
import json
import csv


"""
Converts CSV data downloaded from Hotcrp to JSON for use by website.

Expected inputs:
(1) order:acm:pub list with 'paper information' exported as CSV.
    Format: line 0 is headers, then a line is either:
      * csv paper info
      * begins with a pound sign indicating the session name
(2) order:acm:pub list with 'authors' exported
    Format: line 0 is headers
    each line is one author of one paper

"""



parser = argparse.ArgumentParser(description="Convert Hotcrp CSV to JSON usable by website.  WARNING: the raw input CSV contains non-public data; only make the output publicly visible")
parser.add_argument("csv", metavar="CSV", type=str, help="Download -> Paper Information -> CSV")
parser.add_argument("authors", metavar="AUTHORS", type=str, help="Download -> Paper Information -> Authors")
parser.add_argument("output", metavar="OUT", type=str, help="Output filename")


def load_session_info(csvfile):
	lines = []
	with open(csvfile, "r") as f:
		reader = csv.reader(f)
		for line in reader:
			lines.append(line)

	headers = lines[0]
	sessions = []
	papers = {}
	for line in lines[1:]:
		if len(line) == 1:
			session = {
				"id": len(sessions)+1,
				"name": line[0][1:].strip(),
				"papers": []
			}
			sessions.append(session)
		else:
			paperinfo = dict(zip(headers, line))
			paper = {
				"title": paperinfo["Title"],
				"id": paperinfo["ID"],
			}
			papers[paper["id"]] = paper
			sessions[-1]["papers"].append(paper)

	return sessions, papers

def load_author_info(authors):
	lines = []
	with open(authors, "r") as f:
		reader = csv.reader(f)
		for line in reader:
			lines.append(line)

	headers = lines[0]
	papers = {}

	for line in lines[1:]:
		authorinfo = dict(zip(headers, line))
		paperid = authorinfo["paper"]

		author = {
			"first": authorinfo["first"],
			"last": authorinfo["last"],
			"affiliation": authorinfo["affiliation"]
		}

		if paperid not in papers:
			papers[paperid] = []

		papers[paperid].append(author)

	return papers

def combine(sessions, papers, authors):
	for paperid, authorlist in authors.items():
		papers[paperid]["authors"] = authorlist

	return sessions

def write_output(filename, sessions):
	jsondata = json.dumps(sessions, indent=2)
	with open(filename, "w") as f:
		f.write(jsondata)
	print("%d papers in %d sessions" % (sum([len(session["papers"]) for session in sessions]), len(sessions)))
	print("Wrote %d lines to %s" % (len(jsondata.split("\n")), filename))


def run(args):
	sessions, papers = load_session_info(args.csv)
	authors = load_author_info(args.authors)
	data = combine(sessions, papers, authors)
	write_output(args.output, data)

if __name__ == '__main__':
    args = parser.parse_args()
    exit(run(args))