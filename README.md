Dependencies:
 - Python 3.6+
 - jinja2
 - jinja2-highlight
 - pygments
 - pytz

 `pip3 install jinja2 jinja2-highlight pygments pytz`

# Updating the website:

1. Modifications should be made to the files in the `templates` directory
2. Run `python3 generate.py`
3. Commit and push the changes to the git repo.  The live website will auto-update with about a 30 second delay.

# Checking locally

You can run `python -m SimpleHTTPServer` and go to `http://localhost:8000` to preview any changes before committing.

